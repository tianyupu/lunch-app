var map;
var infowindow;
var sydney;
var atlassianCoords = new google.maps.LatLng(-33.8672590, 151.2070511);

function initialize() {
  sydney = new google.maps.LatLng(-33.8600, 151.2094);

  map = new google.maps.Map(document.getElementById('map-canvas'), {
    center: sydney,
    zoom: 15
  });

  createSpecialMarker(atlassianCoords, "Atlassian", "https://marketplace-cdn.atlassian.com/files/images/5d4f6faa-8a54-4e57-bfed-2044e9e955fd.png");
}

function calculateDistance(origin, place) {
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix(
    {
      origins: [origin],
      destinations: [new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng())],
      travelMode: google.maps.TravelMode.WALKING,
      unitSystem: google.maps.UnitSystem.METRIC,
    }, function(res){
        place.distance = res.rows[0].elements[0].distance.text; 
        place.duration = res.rows[0].elements[0].duration.text;
      });
}

function search(term){
  var request = {
    location: sydney,
    radius: 10000,
    name: term,
    types: ['food']
  };
  infowindow = new google.maps.InfoWindow();
  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch(request, callback);
}

function callback(results, status) {
  if (status == google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
      calculateDistance(atlassianCoords, results[i]);
    }
  }
}

function createSpecialMarker(coords, title, icon){
  var placeLoc = coords;
  var marker = new google.maps.Marker({
    title: title,
    map: map,
    position: coords,
    icon: icon
  });
}

function createMarker(place) {
  var placeLoc = place.geometry.location;
  var marker = new google.maps.Marker({
    map: map,
    title: place.name,
    position: place.geometry.location,
    animation: google.maps.Animation.DROP
  });

  google.maps.event.addListener(marker, 'click', function() {
    infowindow.setContent(
      "<b>"+place.name + 
      "</b></br>Rating: " + place.rating + "/5 </br>"
       + place.vicinity
       + "</br><em>Distance "+ place.distance + " Duration " + place.duration + "</em>"
      + "</br><img src=\"" + place.icon + "\"></img>"
      );
    infowindow.open(map, this);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);