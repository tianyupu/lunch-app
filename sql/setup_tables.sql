DROP table hungry_people_join_events;
DROP table event CASCADE;
DROP TABLE hungry_people;
drop table cuisine;

CREATE TABLE hungry_people(
  name VARCHAR(80) NOT NULL,
  email VARCHAR(80) NOT NULL PRIMARY KEY ,
  password VARCHAR(80) NOT NULL
);

CREATE TABLE event(
  event_ID SERIAL NOT NULL PRIMARY KEY ,
  cuisine VARCHAR(80),
  meet_time TIMESTAMP(6),
  meet_loc VARCHAR(150),
  description VARCHAR(200)
);

CREATE TABLE hungry_people_join_events(
  user_email VARCHAR(80) NOT NULL REFERENCES hungry_people,
  event_ID INTEGER NOT NULL REFERENCES event
);
