#!/usr/bin/env node

var pg = require("pg");

var conString = "pg://localhost:5432/lunchdb";

var client = new pg.Client(conString);
client.connect();

/* 
Function: createLunch 
Operation: takes even details and creates a new entry in the 'event' table
Example usage: createLunch("New food","1","1","pm","here","test");
*/
var createLunch = function (cuisine, hours, minutes, ampm, meet_loc, description) {
    // Create the meet time based on inputted hours minutes etc
    var today = new Date();
    var day = today.getDate();
    if(day<10) {
        day='0'+day;
    } 
    var month = today.getMonth()+1; // January is 0
    if(month<10) {
        month='0'+month;
    } 
    var year = today.getFullYear();
    if(ampm == "pm"){
        hours = +hours + 12;
    }
    if(hours<10){
        hours = '0'+hours;
    }
    /*
    // At the moment, the interface has all minutes with 2 digits
    if(minutes<10){
        minutes = '0'+minutes;
    }
    */
    meet_time = year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + "00";
    console.log(meet_time);
    client.query("INSERT INTO event(cuisine, meet_time, meet_loc, description) values($1, $2, $3, $4)", [cuisine, meet_time, meet_loc, description]);
};

var joinLunch = function (user_email, event_ID) {
    client.query("INSERT INTO hungry_people_join_events(user_email, event_ID) values($1, $2)", [user_email, event_ID]);
};

/*
Create a new user
*/
var createUser = function(uname, email, pass){
    client.query("INSERT INTO event(name, email, password) values($1, $2, $3)", [uname, email, pass]); 
}

/*
Gets the user stuff
For testing:
getUser('kristy', function (user) {
    console.log(user);
});
*/
var getUser = function(user_name, callback){
    var query = client.query("SELECT * FROM hungry_people WHERE name='" + user_name + "'");
    query.on("row", function (row, result) {
        result.addRow(row);
    });
    query.on("end", function (result) {
        user = result.rows
        client.end();
        callback(user);
    });

}

/*
Validate a user when logging in
*/
var validateUser = function(email, pass, callback){
    var query = client.query("SELECT * FROM hungry_people WHERE email='" + email + "'");
    query.on("row", function (row, result) {
        result.addRow(row);
    });
    query.on("end", function (result) {
        if(result.rows[0].password == pass){
            user = JSON.stringify(result.rows, null, "    ");
            client.end();
            callback(user);
        }
    });
}

/*
Function getAllLunch
Operation: return a json blob of the lunch events
Example usage:
getAllLunch(function (lunch) {
    console.log(lunch)
});
*/ 
var getAllLunch = function(callback){
    var lunch;
    var query = client.query("SELECT * FROM event");
    query.on("row", function (row, result) {
        result.addRow(row);
    });
    query.on("end", function (result) {
        lunch = result.rows;
        callback(lunch);
    });
}


/*
var query = client.query("SELECT * FROM event");
query.on("row", function (row, result) {
    result.addRow(row);
});
query.on("end", function (result) {
    console.log(JSON.stringify(result.rows, null, "    "));
    client.end();
});
*/

exports.createLunch = createLunch;
exports.joinLunch = joinLunch;
exports.getUser = getUser;
exports.getAllLunch = getAllLunch;
