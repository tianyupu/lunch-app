# Lunch App

Dependencies: Postgresql, node.js 

## Setting up the web app
Clone the git repository.

Run the files in the sql folder in postgres to create the required database tables

Run `npm install` to install all dependencies, and then `node app.js` to start the app.

Navigate to `localhost:5000` in your nearest browser.