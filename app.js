var express = require('express');
var exphbs = require('express-handlebars');
var bodyParser = require('body-parser');
var request = require('request');
var db = require('./src/db.js')
var app = express();

app.set('port', (process.env.PORT || 5000));
app.set('views', __dirname + '/views');
app.engine('handlebars', exphbs({ layout: false }));
app.set('view engine', 'handlebars');
app.use('/static', express.static(__dirname + '/static'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/hello', function(req, res, next) {
  res.write('<p>Sending a hipchat notification...</p>');
  request.post('https://api.hipchat.com/v2/room/1144705/notification?auth_token=9nLPnDvagm6Q828viHaEQB9lkIeY54BmsrEyz3HZ', {
    body: {
      message: 'Test message notification @TianPu',
      notify: true,
      message_format: 'text'
    },
    json: true
  });
});

app.get('/', function(req, res, next) {
    db.getAllLunch(function (lunch) {
        res.render('index', {events: lunch}, function (err, html) {
            db.getUser("foo", function (user_details) {
                console.log(user_details[0].email)
                res.render('index', {
                    events: lunch,
                    username: user_details[0].name,
                    email: user_details[0].email
                }, function (err, html) {
                    res.send(html);
                })
            });
        });
    });
});

/*
Sign into the web page
*/
app.get('/signin', function(req, res, next){
    if(!req.body.user_email || !req.body.user_pass){
        response.sendStatus(400);
    }
    else{
        db.verifyUser(req.params.user_email, reqs.params.user_pass ,function(user_details){
            res.cookie("username", username, {secure:false});
        });
        db.createUser(req.body.user_name, req.body.user_email, req.body.user_pass);
    }
});



/*
For creating a new event, this function expects there to be:
 cuisine, hours, minutes, ampm, meet_loc, description
 present as fields in the body of the post,
 if not present this method will return a 400 status code (bad request)
 */

app.post('/event/create', function(req, res, next){
  console.log(req.body.hours);
  console.log(req.body.minutes);

  if(!req.body.cuisine ||!req.body.hours||!req.body.minutes||!req.body.ampm || !req.body.meet_loc) {
    res.status(400).send('There was an error');
  } else {
      db.createLunch(req.body.cuisine, req.body.hours, req.body.minutes, req.body.ampm, req.body.meet_loc, req.body.description);
      res.redirect('/');
  }

});

/*
For joining an event, this function expects there to be:
user_email and event_ID
 present as fields in the body of the post,
 if not present this method will return a 400 status code (bad request)
 */
app.post('/event/join', function(req, res, next){
  if(!req.body.user_email ||!req.body.event_ID){
    response.sendStatus(400);
  }
  else{
    db.joinLunch(req.body.user_email, req.body.event_ID)  ;
  }

});

/*
For creating a new user
*/
app.post('/signup', function(req, res, next){
    if(!req.body.user_name || !req.body.user_email || !req.body.user_pass){
        response.sendStatus(400);
    }
    else{
        db.createUser(req.body.user_name, req.body.user_email, req.body.user_pass);
    }
})


/*
For looking at what lunches are available
*/
app.get('/event/lunches', function(req, res, next){
    db.getAllLunch(function (lunch){
        res.send(lunch);
    });
});

/*
Usernames
*/
app.get('/user/:name', function(req, res, next){
    db.getUser(req.params.name,function(user_details){
        res.send(user_details);
    });
});

app.listen(app.get('port'), function() {
  console.log("Lunch app running at localhost:" + app.get('port'));
});
