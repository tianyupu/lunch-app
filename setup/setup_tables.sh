psql --command "CREATE DATABASE lunchdb"
echo "Removing existing tables and creating new tables"
psql --dbname lunchdb --file ../sql/setup_tables.sql
psql --dbname lunchdb --file ../sql/insert_sample_data.sql
